#  Git issue manager

## Description

Allows to manage issues from Gitlab and GitHub. Supports creating, modifying and changing state (open/close) of issue.

![setup page](https://gitlab.com/Phoenix510/git_service_client/-/wikis/uploads/025383e481aa1db06e7c7c94858d0d5c/image.png)

![issues page](https://gitlab.com/Phoenix510/git_service_client/-/wikis/uploads/1c87ddaa1709ccf2dbdc8794c436e6b8/image.png)

![new issue page](https://gitlab.com/Phoenix510/git_service_client/-/wikis/uploads/2ea520eba2b1d7a4b4866bc8e99f1eff/image.png)

![modify issue page](https://gitlab.com/Phoenix510/git_service_client/-/wikis/uploads/dab4824e851048fd60e8418227a7020e/image.png)

Additionally allows to export issues and import it, including cross (e.g Gitlab issues to GitHub) import.

![import/export page](https://gitlab.com/Phoenix510/git_service_client/-/wikis/uploads/2d874030c15add2369d345fab5754c04/image.png)

### Mobile friendly design

![setup page mobile](https://gitlab.com/Phoenix510/git_service_client/-/wikis/uploads/009137b99a9baddb7e532ea149e89949/image.png)

## Why?



## Quick start

- Clone repository or download source code and unpack it. Open project in IDE and run or type dotnet run.

