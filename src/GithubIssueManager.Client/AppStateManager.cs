﻿using AutoMapper;
using Blazored.SessionStorage;
using GitIssueManager.CoreLibrary;
using GitIssueManager.CoreLibrary.GitCommManager;
using GitIssueManager.CoreLibrary.Models;
using Microsoft.AspNetCore.Components;

namespace GitIssueManager.Client
{
    public class AppStateManager
    {
        public bool Connected => CurrentSettings != null;
        public GitSettings? CurrentSettings { get; private set; }
        private readonly IMapper _mapper;
        private readonly ISessionStorageService _sessionStorage;
        private readonly NavigationManager _navigationManager;
        private IGitCommunicationManager? _gitCommunicationManager;
        private const string SettingsKey = "Settings";

        public AppStateManager(IMapper mapper, ISessionStorageService sessionStorage,
            NavigationManager navigationManager)
        {
            _mapper = mapper;
            _sessionStorage = sessionStorage;
            _navigationManager = navigationManager;
            _ = RestoreState();
        }

        private async Task RestoreState()
        {
            CurrentSettings = await _sessionStorage
                                .GetItemAsync<GitSettings>(SettingsKey);
            AssignCommunicationManager(CurrentSettings);
            _navigationManager.NavigateTo("/");
        }

        public async Task ResetState()
        {
            CurrentSettings = null;
            _gitCommunicationManager = null;
            await _sessionStorage.RemoveItemAsync(SettingsKey);
        }

        public IGitCommunicationManager GetGitCommunicationManager()
        {
            return _gitCommunicationManager ?? 
                throw new NullReferenceException("Communication manager is null");
        }

        public IGitCommunicationManager SetCommunicationManager(GitSettings settings)
        {
            AssignCommunicationManager(settings);
            CurrentSettings = settings;
            _sessionStorage.SetItemAsync(SettingsKey, settings);

            return _gitCommunicationManager!;
        }

        private void AssignCommunicationManager(GitSettings settings) 
        {
            var provider = new GitCommManagerProvider(_mapper, settings);
            _gitCommunicationManager = provider.GetManager();
        }
    }
}
