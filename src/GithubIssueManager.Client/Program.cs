using Blazored.SessionStorage;
using GitIssueManager.CoreLibrary.Mapping;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;

namespace GitIssueManager.Client
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");
            builder.RootComponents.Add<HeadOutlet>("head::after");

            builder.Services.AddBlazoredSessionStorageAsSingleton();
            builder.Services.AddSingleton<AppStateManager>();
            builder.Services.AddAutoMapper(typeof(MappingProfiles));

            await builder.Build().RunAsync();
        }
    }
}