﻿using GitIssueManager.CoreLibrary.Models;

namespace GitIssueManager.CoreLibrary.Mapping
{

    internal static class StateMapper
    {
        static Dictionary<IssueState, string> toGithubMap = new Dictionary<IssueState, string>()
        {
            { IssueState.Open, "open" },
            { IssueState.Closed, "closed" },
            { IssueState.Unknown, "open" },
        };

        static Dictionary<string, IssueState> fromGithubMap = new Dictionary<string, IssueState>()
        {
            { "open", IssueState.Open },
            { "closed", IssueState.Closed },
        };

        static Dictionary<IssueState, string> toGitlabMap = new Dictionary<IssueState, string>()
        {
            { IssueState.Open, "opened" },
            { IssueState.Closed, "closed" },
            { IssueState.Unknown, "opened" },
        };

        static Dictionary<string, IssueState> fromGitlabMap = new Dictionary<string, IssueState>()
        {
            { "opened", IssueState.Open },
            { "closed", IssueState.Closed },
        };

        public static IssueState ConvertFromGithubState(string state)
         => fromGithubMap.TryGetValue(state.ToLower(), out IssueState issueState) ? issueState : IssueState.Unknown;

        public static string ConvertToGithubState(IssueState state)
            => toGithubMap.TryGetValue(state, out string githubState) ? githubState : "open";

        public static IssueState ConvertFromGitlabState(string state)
            => fromGitlabMap.TryGetValue(state.ToLower(), out IssueState issueState) ? issueState : IssueState.Unknown;

        public static string ConvertToGitlabState(IssueState state)
            => toGitlabMap.TryGetValue(state, out string githubState) ? githubState : "open";
    }
}
