﻿using AutoMapper;
using GitIssueManager.CoreLibrary.Models;

namespace GitIssueManager.CoreLibrary.Mapping
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Issue, GithubIssue>()
                .ForMember(dest => dest.Title,
                    opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Body,
                    opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.State,
                    opt => opt.MapFrom(src => StateMapper.ConvertToGithubState(src.State)));

            CreateMap<GithubIssue, Issue>()
                .ForMember(dest => dest.Name,
                    opt => opt.MapFrom(src => src.Title))
                .ForMember(dest => dest.Description,
                    opt => opt.MapFrom(src => src.Body))
                .ForMember(dest => dest.State,
                    opt => opt.MapFrom(src => StateMapper.ConvertFromGithubState(src.State)));

            CreateMap<Issue, GitlabIssue>()
                .ForMember(dest => dest.Title,
                    opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.State,
                    opt => opt.MapFrom(src => StateMapper.ConvertToGitlabState(src.State)));

            CreateMap<GitlabIssue, Issue>()
                .ForMember(dest => dest.Name,
                    opt => opt.MapFrom(src => src.Title))
                .ForMember(dest => dest.Number,
                    opt => opt.MapFrom(src => src.IId))
                .ForMember(dest => dest.State,
                    opt => opt.MapFrom(src => StateMapper.ConvertFromGitlabState(src.State)));
        }

    }
}
