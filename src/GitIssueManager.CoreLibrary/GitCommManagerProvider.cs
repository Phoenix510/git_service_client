﻿using AutoMapper;
using GitIssueManager.CoreLibrary.GitCommManager;
using GitIssueManager.CoreLibrary.Models;

namespace GitIssueManager.CoreLibrary
{

    public class GitCommManagerProvider
    {
        private readonly IMapper _mapper;
        private readonly GitSettings _settings;

        public GitCommManagerProvider(IMapper mapper, GitSettings settings)
        {
            _mapper = mapper;
            _settings = settings;
        }

        public IGitCommunicationManager GetManager()
        {
            return _settings.GitService switch
            {
                GitService.GitLab => new GitLabCommManager(_settings.RepositoryId, _mapper, GitLabCommManager.GetHttpClient(_settings.AccessToken)),
                GitService.GitHub => new GitHubCommManager(_settings.RepositoryId, _mapper, GitHubCommManager.GetHttpClient(_settings.AccessToken)),
                _ => throw new NotImplementedException($"Type {_settings.GitService} not implemented"),
            };
        }
    }
}
