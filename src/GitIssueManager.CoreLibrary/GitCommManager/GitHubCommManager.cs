﻿using GitIssueManager.CoreLibrary.Models;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using AutoMapper;
using GitIssueManager.CoreLibrary.GitCommManager.Errors;
using System.Xml.Linq;

namespace GitIssueManager.CoreLibrary.GitCommManager
{

    internal class GitHubCommManager : IGitCommunicationManager
    {
        private readonly HttpClient _httpClient;
        private readonly string _repositoryId;
        private readonly IMapper _mapper;
        private readonly BulkOperations _bulkOperations;
        private const int MaxPageCount = 100;
        private GitErrorHandler<GithubErrorResponse> _errorHandler;

        public event Action<UpdateObject>? Update;

        public GitHubCommManager(string repoId,
            IMapper mapper, HttpClient httpClient)
        {
            _httpClient = httpClient;
            _repositoryId = repoId;
            _mapper = mapper;
            _bulkOperations = new BulkOperations(this);
            _bulkOperations.IssueImportUpdate += ((int, string) data) => Update?.Invoke(new UpdateObject(UpdateType.BulkInsertionUpdate, data));
            _errorHandler = new();
        }

        public static HttpClient GetHttpClient(
            string accessToken, string baseUrl = "https://api.github.com/")
        {
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(baseUrl);
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", accessToken);
            httpClient.DefaultRequestHeaders.Add("X-GitHub-Api-Version", "2022-11-28");

            return httpClient;
        }

        public async Task AddNewIssue(Issue issue)
        {
            var resp = await _httpClient
                .PostAsJsonAsync($"repos/{_repositoryId}/issues",
                    _mapper.Map<GithubIssue>(issue));

            if (!resp.IsSuccessStatusCode)
                await _errorHandler.HandleFailure(resp);
        }

        public async Task<IEnumerable<Issue>> GetAllIssues(int page, int itemsCount)
        {
            // could use etag to check is there anything new - otherwise there's 60s cache by default
            var resp = await _httpClient.GetAsync($"repos/{_repositoryId}/issues?state=all&page={page}&per_page={itemsCount}");
            if (!resp.IsSuccessStatusCode)
                await _errorHandler.HandleFailure(resp);

            var issues = await resp.Content.ReadFromJsonAsync<List<GithubIssue>>();
            return _mapper.Map<IEnumerable<Issue>>(issues);
        }

        public async Task<Issue> GetIssue(long id)
        {
            var resp = await _httpClient.GetAsync($"repos/{_repositoryId}/issues/{id}");
            if (!resp.IsSuccessStatusCode)
                await _errorHandler.HandleFailure(resp);

            var issue = await resp.Content.ReadFromJsonAsync<GithubIssue>();
            return _mapper.Map<Issue>(issue);
        }

        public async Task<Issue> ModifyIssue(long issueNumber, Issue issue)
        {
            var githubIssue = _mapper.Map<GithubIssue>(issue);
            var resp = await _httpClient
                .PatchAsJsonAsync($"repos/{_repositoryId}/issues/{issueNumber}", githubIssue);

            if (!resp.IsSuccessStatusCode)
                await _errorHandler.HandleFailure(resp);

            var modified = await resp.Content.ReadFromJsonAsync<GithubIssue>();
            return _mapper.Map<Issue>(modified);
        }

        public async Task<Issue> ChangeIssueState(long issueNumber, IssueState newState)
        {
            var newStateValue = newState == IssueState.Open ? "Open" : "Closed";
            var resp = await _httpClient
                .PatchAsJsonAsync($"repos/{_repositoryId}/issues/{issueNumber}",
                    new { State = newStateValue });

            if (!resp.IsSuccessStatusCode)
                await _errorHandler.HandleFailure(resp);
            
            var issue = await resp.Content.ReadFromJsonAsync<GithubIssue>();
            return _mapper.Map<Issue>(issue);
        }

        public async Task ImportIssues(string issuesAsJson)
        {
            await _bulkOperations.ImportIssues(issuesAsJson, 5000, 1000);
        }

        public async Task<string> ExportIssues()
        {
            return await _bulkOperations.ExportIssues(MaxPageCount);
        }
    }
}
