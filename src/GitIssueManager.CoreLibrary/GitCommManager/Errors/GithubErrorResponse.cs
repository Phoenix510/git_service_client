﻿namespace GitIssueManager.CoreLibrary.GitCommManager.Errors
{
    public class GithubErrorResponse : GitErrorResponse
    {
        public string Documentation_Url { get; set; }
    }
}
