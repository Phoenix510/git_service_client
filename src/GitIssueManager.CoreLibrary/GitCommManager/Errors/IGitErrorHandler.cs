﻿namespace GitIssueManager.CoreLibrary.GitCommManager.Errors
{
    internal interface IGitErrorHandler
    {
        Task HandleFailure(HttpResponseMessage message);
    }
}