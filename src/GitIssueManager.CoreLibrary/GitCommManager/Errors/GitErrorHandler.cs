﻿using Newtonsoft.Json;

namespace GitIssueManager.CoreLibrary.GitCommManager.Errors
{
    internal class GitErrorHandler<T> : IGitErrorHandler where T : GitErrorResponse
    {
        public async Task HandleFailure(HttpResponseMessage message)
        {
            var content = await message.Content.ReadAsStringAsync();
            Console.WriteLine($"{message.ReasonPhrase} content");

            var errorContent = JsonConvert.DeserializeObject<T>(content);
            if (errorContent is null || string.IsNullOrEmpty(errorContent.Message))
                throw new HttpRequestException(content, null, message.StatusCode);
            throw new HttpRequestException(errorContent.Message, null, message.StatusCode);
        }
    }
}
