﻿namespace GitIssueManager.CoreLibrary.GitCommManager.Errors
{
    public class GitErrorResponse
    {
        public string Message { get; set; }
    }
}
