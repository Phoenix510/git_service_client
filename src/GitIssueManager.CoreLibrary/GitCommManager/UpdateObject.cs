﻿namespace GitIssueManager.CoreLibrary.GitCommManager
{
    public class UpdateObject
    {
        public UpdateObject(UpdateType type, object data)
        {
            Type = type;
            Data = data;
        }

        public UpdateType Type { get; }
        public object Data { get; }
    }
}
