﻿using AutoMapper;
using GitIssueManager.CoreLibrary.GitCommManager.Errors;
using GitIssueManager.CoreLibrary.Models;
using System.Net.Http.Headers;
using System.Net.Http.Json;

namespace GitIssueManager.CoreLibrary.GitCommManager
{
    internal class GitLabCommManager : IGitCommunicationManager
    {
        private readonly HttpClient _httpClient;
        private readonly string _repositoryId;
        private readonly IMapper _mapper;
        private readonly BulkOperations _bulkOperations;
        private const int MaxPageCount = 100;
        private GitErrorHandler<GitErrorResponse> _errorHandler;

        public event Action<UpdateObject>? Update;

        public GitLabCommManager(string repoId, IMapper mapper, HttpClient httpClient)
        {
            _httpClient = httpClient;
            _repositoryId = repoId;
            _mapper = mapper;
            _bulkOperations = new BulkOperations(this);
            _bulkOperations.IssueImportUpdate += ((int, string) data) => Update?.Invoke(new UpdateObject(UpdateType.BulkInsertionUpdate, data));
            _errorHandler = new();
        }

        public static HttpClient GetHttpClient(string accessToken, string baseUrl= "https://gitlab.com/api/v4/")
        {
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(baseUrl);
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", accessToken);
            return httpClient;
        }

        public async Task AddNewIssue(Issue issue)
        {
            var resp = await _httpClient
                .PostAsJsonAsync($"projects/{_repositoryId}/issues",
                    _mapper.Map<GitlabIssue>(issue));

            if (!resp.IsSuccessStatusCode)
                await _errorHandler.HandleFailure(resp);
        }

        public async Task<IEnumerable<Issue>> GetAllIssues(int page, int itemsCount)
        {
            var resp = await _httpClient.GetAsync($"projects/{_repositoryId}/issues?page={page}&per_page={itemsCount}");
            if (!resp.IsSuccessStatusCode)
                await _errorHandler.HandleFailure(resp);

            var issues = await resp.Content
                .ReadFromJsonAsync<List<GitlabIssue>>();
            return _mapper.Map<IEnumerable<Issue>>(issues);
        }

        public async Task<Issue> GetIssue(long issueNum)
        {
            var resp = await _httpClient.GetAsync($"projects/{_repositoryId}/issues/{issueNum}");
            if (!resp.IsSuccessStatusCode)
                await _errorHandler.HandleFailure(resp);

            var issue = await resp.Content
                .ReadFromJsonAsync<GitlabIssue>();
            return _mapper.Map<Issue>(issue);
        }

        public async Task<Issue> ModifyIssue(long issueNumber, Issue issue)
        {
            var gitlabIssue = _mapper.Map<GitlabIssue>(issue);
            var resp = await _httpClient
                .PutAsJsonAsync($"projects/{_repositoryId}/issues/{issueNumber}", gitlabIssue);

            if (!resp.IsSuccessStatusCode)
                await _errorHandler.HandleFailure(resp);

            var modified = await resp.Content.ReadFromJsonAsync<GitlabIssue>();
            return _mapper.Map<Issue>(modified);
        }

        public async Task<Issue> ChangeIssueState(long issueNum, IssueState newState)
        {
            var stateEventValue = newState == IssueState.Open ? "reopen" : "close";
            var resp = await _httpClient
                .PutAsync($"projects/{_repositoryId}/issues/{issueNum}?state_event={stateEventValue}", null);
            
            if (!resp.IsSuccessStatusCode)
                await _errorHandler.HandleFailure(resp);

            var modified = await resp.Content.ReadFromJsonAsync<GitlabIssue>();
            return _mapper.Map<Issue>(modified);
        }

        public async Task<string> ExportIssues()
        {
            return await _bulkOperations.ExportIssues(MaxPageCount);
        }

        public async Task ImportIssues(string issuesAsJson)
        {
            await _bulkOperations.ImportIssues(issuesAsJson, 12000, 500);
        }
    }
}
