﻿using AutoMapper;
using GitIssueManager.CoreLibrary.GitCommManager.Errors;
using GitIssueManager.CoreLibrary.Models;
using System.Net.Http.Headers;

namespace GitIssueManager.CoreLibrary.GitCommManager.v2
{
    internal class GitLabCommManager: IGitCommunicationManager
    {
        private readonly string _repositoryId;
        private readonly IMapper _mapper;
        private readonly BulkOperations _bulkOperations;
        private const int MaxPerPage = 100;
        private readonly WebRequestHandler _webRequestHandler;
        
        public event Action<UpdateObject>? Update;

        public GitLabCommManager(string repoId, IMapper mapper, HttpClient httpClient)
        {
            _repositoryId = repoId;
            _mapper = mapper;
            _bulkOperations = new BulkOperations(this);
            _webRequestHandler = new(mapper,
                new GitErrorHandler<GitErrorResponse>(),
                httpClient);

            _bulkOperations.IssueImportUpdate += ((int, string) data) => Update?.Invoke(new UpdateObject(UpdateType.BulkInsertionUpdate, data));

        }

        public static HttpClient GetHttpClient(string accessToken, string baseUrl = "https://gitlab.com/api/v4/")
        {
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(baseUrl);
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", accessToken);
            return httpClient;
        }

        public async Task AddNewIssue(Issue issue)
        {
            await _webRequestHandler.Change($"projects/{_repositoryId}/issues",
                HttpMethod.Post, _mapper.Map<GitlabIssue>(issue));
        }

        public async Task<IEnumerable<Issue>> GetAllIssues(int page, int itemsCount)
        {
            return await _webRequestHandler.Get<List<GitlabIssue>, IEnumerable<Issue>>
                ($"projects/{_repositoryId}/issues?page={page}&per_page={itemsCount}");
        }

        public async Task<Issue> GetIssue(long issueNum)
        {
            return await _webRequestHandler.Get<GitlabIssue, Issue>
                ($"projects/{_repositoryId}/issues/{issueNum}");
        }

        public async Task<Issue> ModifyIssue(long issueNumber, Issue issue)
        {
            var gitlabIssue = _mapper.Map<GitlabIssue>(issue);
            return await _webRequestHandler
                .Change<GitlabIssue, GitlabIssue, Issue>($"projects/{_repositoryId}/issues/{issueNumber}", 
                    HttpMethod.Put, gitlabIssue);
        }

        public async Task<Issue> ChangeIssueState(long issueNum, IssueState newState)
        {
            var stateEventValue = newState == IssueState.Open ? "reopen" : "close";
            return await _webRequestHandler.Change<object, GitlabIssue, Issue>
                ($"projects/{_repositoryId}/issues/{issueNum}?state_event={stateEventValue}", 
                    HttpMethod.Put, null);
        }

        public async Task<string> ExportIssues()
        {
            return await _bulkOperations.ExportIssues(MaxPerPage);
        }

        public async Task ImportIssues(string issuesAsJson)
        {
            await _bulkOperations.ImportIssues(issuesAsJson, 12000, 500);
        }
    }
}
