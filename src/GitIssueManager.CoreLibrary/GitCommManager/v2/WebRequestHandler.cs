﻿using AutoMapper;
using GitIssueManager.CoreLibrary.GitCommManager.Errors;
using Newtonsoft.Json;
using System.Net.Http.Json;
using System.Text;

namespace GitIssueManager.CoreLibrary.GitCommManager.v2
{
    internal class WebRequestHandler
    {
        private readonly IMapper _mapper;
        private IGitErrorHandler _errorHandler;
        private readonly HttpClient _httpClient;

        public WebRequestHandler(
            IMapper mapper,
            IGitErrorHandler gitErrorHandler,
            HttpClient httpClient)
        {
            _mapper = mapper;
            _errorHandler = gitErrorHandler;
            _httpClient = httpClient;
        }

        //public async Task Change(Func<Task<HttpResponseMessage>> changeFunc)
        //{
        //    var resp = await changeFunc();
        //    if (!resp.IsSuccessStatusCode)
        //        await _errorHandler.HandleFailure(resp);
        //}

        //public async Task<TOut> Get<TRead, TOut>(Func<Task<HttpResponseMessage>> getFunc)
        //{
        //    var resp = await getFunc();
        //    if (!resp.IsSuccessStatusCode)
        //        await _errorHandler.HandleFailure(resp);

        //    var data = await resp.Content.ReadFromJsonAsync<TRead>();
        //    return _mapper.Map<TOut>(data);
        //}

        public async Task<TOut> Get<TRead, TOut>(string url)
        {
            var resp = await _httpClient.GetAsync(new Uri(url, UriKind.Relative));
            if (!resp.IsSuccessStatusCode)
                await _errorHandler.HandleFailure(resp);

            var data = await resp.Content.ReadFromJsonAsync<TRead>();
            return _mapper.Map<TOut>(data);
        }

        public async Task Change<T>(string url, HttpMethod methodType, T contentObj) 
            where T : class
        {
            var requestMessage = new HttpRequestMessage(methodType, new Uri(url, UriKind.Relative));
            requestMessage.Content = new StringContent
                (JsonConvert.SerializeObject(contentObj), Encoding.UTF8, "application/json");

            var resp = await _httpClient.SendAsync(requestMessage);
            if (!resp.IsSuccessStatusCode)
                await _errorHandler.HandleFailure(resp);
        }

        public async Task<TOut> Change<TIn, TRead, TOut>(string url, HttpMethod methodType, TIn contentObj)
            where TIn : class
        {
            var requestMessage = new HttpRequestMessage(methodType, new Uri(url, UriKind.Relative));
            requestMessage.Content = new StringContent
                (JsonConvert.SerializeObject(contentObj), Encoding.UTF8, "application/json");

            var resp = await _httpClient.SendAsync(requestMessage);
            if (!resp.IsSuccessStatusCode)
                await _errorHandler.HandleFailure(resp);

            var data = await resp.Content.ReadFromJsonAsync<TRead>();
            return _mapper.Map<TOut>(data);
        }
    }
}
