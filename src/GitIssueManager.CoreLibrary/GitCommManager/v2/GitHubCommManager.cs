﻿using GitIssueManager.CoreLibrary.Models;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using AutoMapper;
using GitIssueManager.CoreLibrary.GitCommManager.Errors;

namespace GitIssueManager.CoreLibrary.GitCommManager.v2
{

    internal class GitHubCommManager : IGitCommunicationManager
    {
        private readonly string _repositoryId;
        private readonly IMapper _mapper;
        private const int MaxPerPage = 100;
        private readonly WebRequestHandler _webRequestHandler;
        private readonly BulkOperations _bulkOperations;

        public event Action<UpdateObject>? Update;


        public GitHubCommManager(string repoId,
            IMapper mapper, HttpClient httpClient)
        {
            _repositoryId = repoId;
            _mapper = mapper;
            _bulkOperations = new(this);
            _webRequestHandler = new(mapper,
                new GitErrorHandler<GithubErrorResponse>(),
                httpClient);

            _bulkOperations.IssueImportUpdate += ((int, string) data) => Update?.Invoke(new UpdateObject(UpdateType.BulkInsertionUpdate, data));

        }

        public static HttpClient GetHttpClient(
            string accessToken, string baseUrl = "https://api.github.com/")
        {
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(baseUrl);
            httpClient.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", accessToken);
            httpClient.DefaultRequestHeaders.Add("X-GitHub-Api-Version", "2022-11-28");

            return httpClient;
        }

        public async Task AddNewIssue(Issue issue)
        {
            await _webRequestHandler.Change($"repos/{_repositoryId}/issues",
                HttpMethod.Post, _mapper.Map<GithubIssue>(issue));
        }

        public async Task<IEnumerable<Issue>> GetAllIssues(int page, int itemsCount)
        {
            // could use etag to check is there anything new - otherwise there's 60s cache by default
            return await _webRequestHandler.Get<List<GithubIssue>, IEnumerable<Issue>>
                ($"repos/{_repositoryId}/issues?state=all&page={page}&per_page={itemsCount}");


            //return await _webRequestHandler.Get<List<GithubIssue>, IEnumerable<Issue>>
            //    (async () => await _httpClient.GetAsync
            //        ($"repos/{_repositoryId}/issues?state=all&page={page}&per_page={itemsCount}"));
        }

        public async Task<Issue> GetIssue(long id)
        {
            return await _webRequestHandler.Get<GithubIssue, Issue>
                ($"repos/{_repositoryId}/issues/{id}");
        }

        public async Task<Issue> ModifyIssue(long issueNumber, Issue issue)
        {
            var githubIssue = _mapper.Map<GithubIssue>(issue);
            return await _webRequestHandler
                 .Change<object, GithubIssue, Issue>($"repos/{_repositoryId}/issues/{issueNumber}", HttpMethod.Patch, githubIssue);
        }

        public async Task<Issue> ChangeIssueState(long issueNumber, IssueState newState)
        {
            var newStateValue = newState == IssueState.Open ? "Open" : "Closed";
            return await _webRequestHandler
                .Change<object, GithubIssue, Issue>($"repos/{_repositoryId}/issues/{issueNumber}",
                HttpMethod.Patch, new { state = newStateValue });
        }

        public async Task ImportIssues(string issuesAsJson)
        {
            await _bulkOperations.ImportIssues(issuesAsJson, 5000, 1000);
        }

        public async Task<string> ExportIssues()
        {
            return await _bulkOperations.ExportIssues(MaxPerPage);
        }
    }
}
