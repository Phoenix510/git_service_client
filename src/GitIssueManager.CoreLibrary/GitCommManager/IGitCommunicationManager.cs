﻿using GitIssueManager.CoreLibrary.Models;
using Newtonsoft.Json;

namespace GitIssueManager.CoreLibrary.GitCommManager
{
    public interface IGitCommunicationManager
    {
        Task<IEnumerable<Issue>> GetAllIssues(int page, int itemsCount);
        Task<Issue> GetIssue(long id);
        Task AddNewIssue(Issue issue);
        Task<Issue> ModifyIssue(long issueNum, Issue issue);
        Task<Issue> ChangeIssueState(long issueNum, IssueState newState);
        Task ImportIssues(string issuesAsJson);
        Task<string> ExportIssues();
        event Action<UpdateObject> Update;
    }
}
