﻿using System.Net;

namespace GitIssueManager.CoreLibrary.Exceptions
{
    public class BulkInsertException : HttpRequestException
    {
        public BulkInsertException(string? message, Exception? inner, int lastIndex) : base(message, inner)
        {
            LastIndex = lastIndex;
        }

        public BulkInsertException(string? message, Exception? inner, HttpStatusCode? statusCode, int lastIndex) : base(message, inner, statusCode)
        {
            LastIndex = lastIndex;
        }

        public int LastIndex { get; }

        public override string ToString()
        {
            return $"{Message}. Last successfully inserted item index: {LastIndex}";
        }
    }
}
