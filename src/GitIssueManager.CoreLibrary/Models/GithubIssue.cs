﻿using Newtonsoft.Json;

namespace GitIssueManager.CoreLibrary.Models
{
    internal class GithubIssue
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("body")]
        public string Body { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("number")]
        public int Number { get; set; }

        public GithubIssue() { }

        public GithubIssue(string title, string body)
        {
            Title = title;
            Body = body;
        }
    }
}
