﻿namespace GitIssueManager.CoreLibrary.Models
{
    public enum IssueState
    {
        Open,
        Closed,
        Unknown
    }
}
