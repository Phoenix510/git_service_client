﻿using System.ComponentModel.DataAnnotations;

namespace GitIssueManager.CoreLibrary.Models
{
    public class GitSettings
    {
        [Required]
        public GitService GitService { get; set; }
        [Required]
        public string RepositoryId { get; set; }
        [Required]
        public string AccessToken { get; set; }
    }
}
