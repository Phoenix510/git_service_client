﻿namespace GitIssueManager.CoreLibrary.Models
{
    public class Issue
    {
        public Issue() { }

        public Issue(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public long Id { get; set; }
        public long Number { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IssueState State { get; set; } = IssueState.Unknown;
    }
}
