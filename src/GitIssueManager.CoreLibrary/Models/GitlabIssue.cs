﻿using Newtonsoft.Json;

namespace GitIssueManager.CoreLibrary.Models
{
    internal class GitlabIssue
    {
        [JsonProperty("id")]
        public long Id { get; set; }
        [JsonProperty("iid")]
        public long? IId { get; set; }
        [JsonProperty("title")]
        public string? Title { get; set; }
        [JsonProperty("description")]
        public string? Description { get; set; }
        [JsonProperty("state")]
        public string? State { get; set; }

        public GitlabIssue(string title, string description)
        {
            Title = title;
            Description = description;
        }

        public GitlabIssue()
        {
        }
    }
}
