﻿using GitIssueManager.CoreLibrary.Exceptions;
using GitIssueManager.CoreLibrary.GitCommManager;
using GitIssueManager.CoreLibrary.Models;
using Newtonsoft.Json;
using System.Net;

namespace GitIssueManager.CoreLibrary
{

    public class BulkOperations
    {
        private readonly IGitCommunicationManager _communicationManager;
        public event Action<(int, string)>? IssueImportUpdate;

        // we can insert serializer as some common interaface to support any type of data format instead of only json
        public BulkOperations(IGitCommunicationManager communicationManager)
        {
            _communicationManager = communicationManager;
        }

        public async Task ImportIssues(string issuesAsJson, int hourLimit, int millisecondsDelay = 1000, int secondsRetryDelay = 60)
        {
            var issues = JsonConvert.DeserializeObject<List<Issue>>(issuesAsJson)
                ?? new List<Issue>();

            if (issues.Count > hourLimit)
            {
                throw new BulkInsertException($"You're trying to import more issues then service allows per hour. Max {hourLimit}", null, 0);
            }

            int startAt = 0;

            // max add rate per minute might be a problem e.g. for github 80 per minute 500 per hour
            // if we hit limit wait and retry add issues from where stopped last time
            while (true)
            {
                try
                {
                    await ImportIssues(issues, millisecondsDelay, startAt);
                    break;
                }
                catch (BulkInsertException ex) when (ex.StatusCode == HttpStatusCode.Forbidden)
                {
                    // it means we hit limit rate twice on the same item even if we wait or at first item
                    // hour limit or other limit might be exceeded
                    if (startAt == ex.LastIndex)
                    {
                        throw;
                    }

                    // we hit limit wait x seconds and try again + skip already processed items
                    await Task.Delay(TimeSpan.FromSeconds(secondsRetryDelay));
                    startAt = ex.LastIndex;
                }
            }
        }

        private async Task ImportIssues(List<Issue> issues, int millisecondsDelay, int startAt = 0)
        {
            for (int i = startAt; i < issues.Count; i++)
            {
                try
                {
                    await _communicationManager.AddNewIssue(issues[i]);
                    IssueImportUpdate?.Invoke((i, issues[i].Name));
                    // delay to avoid secondary limit - e.g at least one second is advised
                    // in github docs for many post requests in a row
                    await Task.Delay(millisecondsDelay);
                }
                catch (HttpRequestException ex)
                {
                    throw new BulkInsertException(ImportErrorTemplate(ex.Message, i), ex, ex.StatusCode, i);
                }
                catch (Exception ex)
                {
                    throw new BulkInsertException(ImportErrorTemplate(ex.Message, i), ex, i);
                }
            }
        }

        private string ImportErrorTemplate(string message, int index) =>
            $"{message}. Execution stops at item with index {index}";

        public async Task<string> ExportIssues(int maxItemsPerPage = 40)
        {
            List<Issue> allIssues = new();
            int page = 0;
            while (true)
            {
                page++;
                var issues = await _communicationManager.GetAllIssues(page, maxItemsPerPage);
                allIssues.AddRange(issues);
                if (issues.Count() < maxItemsPerPage)
                    break;
            }

            return JsonConvert.SerializeObject(allIssues);
        }
    }
}
