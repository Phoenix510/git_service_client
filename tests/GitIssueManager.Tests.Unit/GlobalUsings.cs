global using Xunit;
global using GitIssueManager.CoreLibrary.GitCommManager;
global using GitIssueManager.CoreLibrary.Models;
global using GitIssueManager.CoreLibrary;
global using NSubstitute;
global using FluentAssertions;
global using NSubstitute.ExceptionExtensions;