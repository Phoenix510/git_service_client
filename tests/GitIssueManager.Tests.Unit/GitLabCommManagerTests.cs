﻿using AutoMapper;
using GitIssueManager.CoreLibrary.Models;
using Newtonsoft.Json;
using RichardSzalay.MockHttp;
using System.Net;

namespace GitIssueManager.Tests.Unit
{
    public class GitLabCommManagerTests
    {
        const string _repositoryId = "test";
        const string _url = "https://gittest.example";

        [Fact]
        public void GetHttpClient_Should_return_HttpClient_with_base_url_and_auth_header_set_When_valid_init_data_is_provided()
        {
            string baseAddress = "https://example.com/api";
            string token = "token";
            HttpClient result = GitLabCommManager
                .GetHttpClient(token, baseAddress);

            result.BaseAddress.Should().Be(baseAddress);
            var authHeader = result.DefaultRequestHeaders.Authorization;
            authHeader.Should().NotBeNull();
            authHeader!.Scheme.Should().Be("Bearer");
            authHeader.Parameter.Should().Be(token);
        }

        [Fact]
        public async Task AddNewIssue_Should_call_valid_endpoint()
        {
            var mapper = Substitute.For<IMapper>();
            var mockHttp = new MockHttpMessageHandler();
            mockHttp.When(HttpMethod.Post, $"{_url}/projects/{_repositoryId}/issues")
                    .Respond(HttpStatusCode.Created);
            HttpClient client = SetupClient(mockHttp);

            var sut = new GitLabCommManager(_repositoryId, mapper, client);

            // if no exception is thrown it means valid endpoint was hit
            await sut.AddNewIssue(new Issue());
        }

        [Fact]
        public async Task ModifyIssue_Should_call_valid_endpoint()
        {
            int issueNumber = 1;
            var mapper = Substitute.For<IMapper>();
            var toSend = new GitlabIssue() { Id = 1 };
            mapper.Map<GitlabIssue>(Arg.Is<Issue>(x => x.Id == 1)).Returns(toSend);
            var mockHttp = new MockHttpMessageHandler();
            mockHttp.When(HttpMethod.Put, $"{_url}/projects/{_repositoryId}/issues/{issueNumber}")
                    .WithContent("{\"id\":1,\"title\":null,\"description\":null,\"state\":null}")
                    .Respond(HttpStatusCode.OK);
            HttpClient client = SetupClient(mockHttp);

            var sut = new GitLabCommManager(_repositoryId, mapper, client);

            // if no exception is thrown it means valid endpoint was hit
            await sut.ModifyIssue(issueNumber, new Issue() { Id = 1 });
        }

        [Fact]
        public async Task CloseIssue_Should_call_valid_endpoint()
        {
            int issueNumber = 1;
            var mapper = Substitute.For<IMapper>();
            var mockHttp = new MockHttpMessageHandler();
            mockHttp.When(HttpMethod.Put, $"{_url}/projects/{_repositoryId}/issues/{issueNumber}?state_event=close")
                    .Respond(HttpStatusCode.OK);
            HttpClient client = SetupClient(mockHttp);

            var sut = new GitLabCommManager(_repositoryId, mapper, client);

            // if no exception is thrown it means valid endpoint was hit
            await sut.CloseIssue(issueNumber);
        }

        [Fact]
        public async Task GetIssue_Should_call_valid_endpoint_and_return_issue()
        {
            int issueNumber = 1;
            var testIssue = new Issue { Id = 1 };
            var mapper = Substitute.For<IMapper>();
            mapper.Map<Issue>(Arg.Is<GitlabIssue>(x => x.Id==1)).Returns(testIssue);
            var mockHttp = new MockHttpMessageHandler();
            mockHttp.When(HttpMethod.Get, $"{_url}/projects/{_repositoryId}/issues/{issueNumber}")
                    .Respond(HttpStatusCode.OK, "application/json", "{\"id\" : 1}");
            HttpClient client = SetupClient(mockHttp);

            var sut = new GitLabCommManager(_repositoryId, mapper, client);

            // if no exception is thrown it means valid endpoint was hit
            Issue result = await sut.GetIssue(issueNumber);

            result.Should().BeEquivalentTo(testIssue);
        }

        [Fact]
        public async Task GetAllIssues_Should_call_valid_endpoint_and_return_issues()
        {
            int page = 1;
            int perPage = 2;
            var testIssue = new Issue { Id = 1 };
            var mapper = Substitute.For<IMapper>();
            mapper.Map<IEnumerable<Issue>>(Arg.Is<List<GitlabIssue>>(x=> x.Count ==1 && x[0].Id == 1)).Returns(new Issue[] { testIssue });
            var mockHttp = new MockHttpMessageHandler();
            mockHttp.When(HttpMethod.Get, $"{_url}/projects/{_repositoryId}/issues?page={page}&per_page={perPage}")
                    .Respond(HttpStatusCode.OK, "application/json", "[{\"id\" : 1}]");
            HttpClient client = SetupClient(mockHttp);

            var sut = new GitLabCommManager(_repositoryId, mapper, client);

            // if no exception is thrown it means valid endpoint was hit
            var result = await sut.GetAllIssues(page, perPage);

            result.Should().HaveCount(1);
            result.First().Should().BeEquivalentTo(testIssue);
        }

        private HttpClient SetupClient(MockHttpMessageHandler mockHttpMessageHandler)
        {
            var client = new HttpClient(mockHttpMessageHandler);
            client.BaseAddress = new Uri(_url);
            return client;
        }
    }
}
