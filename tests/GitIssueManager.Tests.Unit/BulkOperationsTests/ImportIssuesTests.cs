﻿using Newtonsoft.Json;
using GitIssueManager.CoreLibrary.Exceptions;

namespace GitIssueManager.Tests.Unit.BulkOperationsTests
{
    public class ImportIssuesTests
    {
        private string GetRandomIssuesAsJson(int count) =>
            JsonConvert.SerializeObject(Enumerable.Range(1, count)
                       .Select(_ => new Issue()));

        [Fact]
        public async Task ImportIssues_Should_throw_BulkInsertException_When_issues_count_exceeds_hour_limit()
        {
            var communicationManager = Substitute.For<IGitCommunicationManager>();
            var sut = new BulkOperations(communicationManager);
            var issuesAsJson = GetRandomIssuesAsJson(5);
            int maxIssuesPerHour = 4;

            var importAction = async () => await sut.ImportIssues(issuesAsJson, maxIssuesPerHour, 0, 0);
            await importAction.Should()
                .ThrowAsync<BulkInsertException>().WithMessage($"You're trying to import more issues then service allows per hour. Max {maxIssuesPerHour}");
        }

        [Fact]
        public async Task ImportIssues_Should_throw_BulkInsertException_When_Insert_was_rejected_twice_due_to_limit()
        {
            var communicationManager = Substitute.For<IGitCommunicationManager>();
            var error = new HttpRequestException("Limit exceeded", null, System.Net.HttpStatusCode.Forbidden);
            communicationManager.AddNewIssue(Arg.Any<Issue>()).Returns
                (x => Task.CompletedTask, x => { throw error; }, x => { throw error; });
            var sut = new BulkOperations(communicationManager);
            var issuesAsJson = GetRandomIssuesAsJson(3);

            var importAction = async () => await sut.ImportIssues(issuesAsJson, 10, 0, 0);
            await importAction.Should()
                .ThrowAsync<BulkInsertException>()
                .WithMessage("Limit exceeded. Execution stops at item with index 1");
            await communicationManager.Received(3).AddNewIssue(Arg.Any<Issue>());
        }

        [Fact]
        public async Task ImportIssues_Should_throw_BulkInsertException_When_Insert_was_rejected_for_first_issue_due_to_limit()
        {
            var communicationManager = Substitute.For<IGitCommunicationManager>();
            communicationManager.AddNewIssue(Arg.Any<Issue>())
                .Throws(new HttpRequestException("Limit exceeded", null, System.Net.HttpStatusCode.Forbidden));
            var sut = new BulkOperations(communicationManager);
            var issuesAsJson = GetRandomIssuesAsJson(3);

            var importAction = async () => await sut.ImportIssues(issuesAsJson, 10, 0, 0);
            await importAction.Should()
                .ThrowAsync<BulkInsertException>()
                .WithMessage("Limit exceeded. Execution stops at item with index 0");
            await communicationManager.Received(1).AddNewIssue(Arg.Any<Issue>());
        }

        [Fact]
        public async Task ImportIssues_Should_retry_insert_after_specified_time_When_Insert_was_rejected_due_to_limit()
        {
            var communicationManager = Substitute.For<IGitCommunicationManager>();
            var error = new HttpRequestException("Limit exceeded", null, System.Net.HttpStatusCode.Forbidden);
            communicationManager.AddNewIssue(Arg.Any<Issue>()).Returns
                (x => Task.CompletedTask, x => { throw error; }, x => Task.CompletedTask, x=> Task.CompletedTask);
            var sut = new BulkOperations(communicationManager);
            var issuesAsJson = GetRandomIssuesAsJson(3);
            int retryDelayInSeconds = 1;

            // I had no better idea how to check if delay was applied
            var watch = System.Diagnostics.Stopwatch.StartNew();
            await sut.ImportIssues(issuesAsJson, 10, 0, retryDelayInSeconds);
            watch.Stop();

            watch.ElapsedMilliseconds.Should().BeGreaterThanOrEqualTo(retryDelayInSeconds * 1000);
            await communicationManager.Received(4).AddNewIssue(Arg.Any<Issue>());
        }

        [Fact]
        public async Task ImportIssues_Should_wait_specified_time_between_inserts()
        {
            var communicationManager = Substitute.For<IGitCommunicationManager>();
            var sut = new BulkOperations(communicationManager);
            var issuesAsJson = GetRandomIssuesAsJson(2);
            int insertDelayInMilliseconds = 1000;

            // I had no better idea how to check if delay was applied
            var watch = System.Diagnostics.Stopwatch.StartNew();
            await sut.ImportIssues(issuesAsJson, 10, insertDelayInMilliseconds, 0);
            watch.Stop();

            watch.ElapsedMilliseconds.Should().BeGreaterThanOrEqualTo(insertDelayInMilliseconds);
            await communicationManager.Received(2).AddNewIssue(Arg.Any<Issue>());
        }

        [Fact]
        public async Task ImportIssues_Should_throw_BulkInsertException_with_index_When_any_error_is_thrown_during_insert()
        {
            var communicationManager = Substitute.For<IGitCommunicationManager>();
            communicationManager.AddNewIssue(Arg.Any<Issue>()).Throws(new Exception("Error during insert"));
            var sut = new BulkOperations(communicationManager);
            var issuesAsJson = GetRandomIssuesAsJson(2);

            var importAction = async () => await sut.ImportIssues(issuesAsJson, 10, 0, 0);
            await importAction.Should()
                .ThrowAsync<BulkInsertException>()
                .WithMessage("Error during insert. Execution stops at item with index 0");
        }

        [Fact]
        public async Task ImportIssues_Should_insert_all_issues()
        {
            var communicationManager = Substitute.For<IGitCommunicationManager>();
            var sut = new BulkOperations(communicationManager);
            var issuesAsJson = GetRandomIssuesAsJson(8);

            await sut.ImportIssues(issuesAsJson, 10, 0, 0);
            await communicationManager.Received(8).AddNewIssue(Arg.Any<Issue>());
        }
    }
}
