﻿using Newtonsoft.Json;

namespace GitIssueManager.Tests.Unit.BulkOperationsTests
{
    public class ExportIssuesTests
    {
        [Fact]
        public async Task ExportIssues_Should_return_all_issues_in_json_When_there_are_any()
        {
            int perPage = 5;
            var communicationManager = Substitute.For<IGitCommunicationManager>();
            communicationManager.GetAllIssues(1, perPage).Returns(Enumerable.Range(1, perPage)
                       .Select(_ => new Issue())
                       .ToList());
            communicationManager.GetAllIssues(2, perPage).Returns(Enumerable.Range(1, 2)
                       .Select(_ => new Issue())
                       .ToList());
            var sut = new BulkOperations(communicationManager);

            string result = await sut.ExportIssues(perPage);

            var exportedIssues = JsonConvert.DeserializeObject<List<Issue>>(result);
            exportedIssues.Should().HaveCount(7);
        }

        [Fact]
        public async Task ExportIssues_Should_empty_json_array_When_there_are_no_issues()
        {
            var communicationManager = Substitute.For<IGitCommunicationManager>();
            communicationManager.GetAllIssues(Arg.Any<int>(), Arg.Any<int>())
                .ReturnsForAnyArgs(new Issue[0]);
            var sut = new BulkOperations(communicationManager);

            string result = await sut.ExportIssues(5);

            var exportedIssues = JsonConvert.DeserializeObject<List<Issue>>(result);
            exportedIssues.Should().BeEmpty();
        }
    }
}
