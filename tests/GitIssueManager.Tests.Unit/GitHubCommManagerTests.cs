﻿using AutoMapper;
using RichardSzalay.MockHttp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GitIssueManager.Tests.Unit
{
    public class GitHubCommManagerTests
    {
        const string _repositoryId = "test";
        const string _url = "https://gittest.example";

        [Fact]
        public void GetHttpClient_Should_return_HttpClient_with_base_url_and_auth_header_set_When_valid_init_data_is_provided()
        {
            string baseAddress = _url;
            string token = "token";
            HttpClient result = GitHubCommManager
                .GetHttpClient(token, baseAddress);

            result.BaseAddress.Should().Be(baseAddress);
            var authHeader = result.DefaultRequestHeaders.Authorization;
            authHeader.Should().NotBeNull();
            authHeader!.Scheme.Should().Be("Bearer");
            authHeader.Parameter.Should().Be(token);
            var versionHeader = result.DefaultRequestHeaders.GetValues("X-GitHub-Api-Version");
            versionHeader.Should().NotBeNull();
            versionHeader.Should().HaveCount(1);
        }

        [Fact]
        public async Task AddNewIssue_Should_call_valid_endpoint()
        {
            var mapper = Substitute.For<IMapper>();
            var mockHttp = new MockHttpMessageHandler();
            mockHttp.When(HttpMethod.Post, $"{_url}/repos/{_repositoryId}/issues")
                    .Respond(HttpStatusCode.Created);
            HttpClient client = SetupClient(mockHttp);

            var sut = new GitHubCommManager(_repositoryId, mapper, client);

            // if no exception is thrown it means valid endpoint was hit
            await sut.AddNewIssue(new Issue());
        }

        [Fact]
        public async Task ModifyIssue_Should_call_valid_endpoint()
        {
            int issueNumber = 1;
            var mapper = Substitute.For<IMapper>();
            var toSend = new GithubIssue() { Id = 1 };
            mapper.Map<GithubIssue>(Arg.Is<Issue>(x => x.Id == 1)).Returns(toSend);
            var mockHttp = new MockHttpMessageHandler();
            mockHttp.When(HttpMethod.Patch, $"{_url}/repos/{_repositoryId}/issues/{issueNumber}")
                    .WithContent("{\"id\":1,\"title\":null,\"body\":null,\"state\":null}")
                    .Respond(HttpStatusCode.OK);
            HttpClient client = SetupClient(mockHttp);

            var sut = new GitHubCommManager(_repositoryId, mapper, client);

            // if no exception is thrown it means valid endpoint was hit
            await sut.ModifyIssue(issueNumber, new Issue() { Id = 1 });
        }

        [Fact]
        public async Task CloseIssue_Should_call_valid_endpoint()
        {
            int issueNumber = 1;
            var mapper = Substitute.For<IMapper>();
            var mockHttp = new MockHttpMessageHandler();
            mockHttp.When(HttpMethod.Patch, $"{_url}/repos/{_repositoryId}/issues/{issueNumber}")
                    .WithContent("{\"state\":\"Closed\"}")
                    .Respond(HttpStatusCode.OK);
            HttpClient client = SetupClient(mockHttp);

            var sut = new GitHubCommManager(_repositoryId, mapper, client);

            // if no exception is thrown it means valid endpoint was hit
            await sut.CloseIssue(issueNumber);
        }

        [Fact]
        public async Task GetIssue_Should_call_valid_endpoint_and_return_issue()
        {
            int issueNumber = 1;
            var testIssue = new Issue { Id = 1 };
            var mapper = Substitute.For<IMapper>();
            mapper.Map<Issue>(Arg.Is<GithubIssue>(x => x.Id == 1)).Returns(testIssue);
            var mockHttp = new MockHttpMessageHandler();
            mockHttp.When(HttpMethod.Get, $"{_url}/repos/{_repositoryId}/issues/{issueNumber}")
                    .Respond(HttpStatusCode.OK, "application/json", "{\"id\" : 1}");
            HttpClient client = SetupClient(mockHttp);

            var sut = new GitHubCommManager(_repositoryId, mapper, client);

            // if no exception is thrown it means valid endpoint was hit
            Issue result = await sut.GetIssue(issueNumber);

            result.Should().BeEquivalentTo(testIssue);
        }

        [Fact]
        public async Task GetAllIssues_Should_call_valid_endpoint_and_return_issues()
        {
            int page = 1;
            int perPage = 2;
            var testIssue = new Issue { Id = 1 };
            var mapper = Substitute.For<IMapper>();
            mapper.Map<IEnumerable<Issue>>(Arg.Is<List<GithubIssue>>(x => x.Count == 1 && x[0].Id == 1)).Returns(new Issue[] { testIssue });
            var mockHttp = new MockHttpMessageHandler();
            mockHttp.When(HttpMethod.Get, $"{_url}/repos/{_repositoryId}/issues?state=all&page={page}&per_page={perPage}")
                    .Respond(HttpStatusCode.OK, "application/json", "[{\"id\" : 1}]");
            HttpClient client = SetupClient(mockHttp);

            var sut = new GitHubCommManager(_repositoryId, mapper, client);

            // if no exception is thrown it means valid endpoint was hit
            var result = await sut.GetAllIssues(page, perPage);

            result.Should().HaveCount(1);
            result.First().Should().BeEquivalentTo(testIssue);
        }

        private HttpClient SetupClient(MockHttpMessageHandler mockHttpMessageHandler)
        {
            var client = new HttpClient(mockHttpMessageHandler);
            client.BaseAddress = new Uri(_url);
            return client;
        }
    }
}
